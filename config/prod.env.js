const version = require('./version')
module.exports = {
  VERSION: version.VERSION,
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  BASE_API: '"https://api.admin.tobsale.com/"',
  QINIU_SITE: '"https://qiniu.tobsale.com/"',
  QINIU_UPLOAD_SITE: '"https://up-z2.qiniup.com/"'
}
