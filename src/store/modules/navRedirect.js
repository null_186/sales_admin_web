/**
 * 导航跳转时 传递数据
 * module: {
 *    supplierEdit: {
 *        supplier_id: 234
 *    }
 * }
 */
const navRedirect = {
  state: {
    module: {
      supplier: { id: 0 },
      category: { id: 0 },
      goods: { id: 0 },
      slide: { id: 0 },
      banner: { id: 0 },
      coupon: { id: 0 },
      order: { id: 0 },
      coin: { id: 0 },
      agency: { id: 0 },
      clientInfo: { id: 0 }
    }
  }
}

export default navRedirect
