import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

/** note: Submenu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: false
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
      }
    ]
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  { path: '*', redirect: '/404', hidden: true },
  {
    path: '/user',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: 'list',
    meta: {
      title: '用户管理', icon: 'peoples'
    },
    children: [
      { path: 'list', component: () => import('@/views/user/list'), name: 'UserList', meta: { title: '用户' }, noCache: true },
      { path: '/user/info/:id(\\d+)', component: () => import('@/views/user/info'), name: 'UserInfo', meta: { title: '用户信息' }, noCache: false, hidden: true },
      { path: '/user/clientlist', component: () => import('@/views/user/clientinfolist'), name: 'Clientinfolist', meta: { title: '客户列表' }, noCache: true },
      { path: '/user/clientqzlist/:id(\\d+)', component: () => import('@/views/user/clientQzList'), name: 'ClientQzList', meta: { title: '客户圈子' }, noCache: false, hidden: true },
      { path: '/user/jubao', component: () => import('@/views/user/jubao'), name: 'Jubao', meta: { title: '用户举报' }, noCache: false }
    ]
  },
  {
    path: '/vipproduct/productList',
    component: Layout,
    alwaysShow: false, // will always show the root menu
    redirect: 'list',
    meta: {
      title: '会员产品', icon: 'shopping'
    },
    children: [
      { path: '/vipproduct/productList', component: () => import('@/views/vipproduct/productList'), name: 'ProductList', meta: { title: '会员产品' }, noCache: true },
      { path: '/vipproduct/userRight', component: () => import('@/views/vipproduct/userRight'), name: 'UserRight', meta: { title: '用户权益' }, noCache: true },
      { path: '/vipproduct/companyRight', component: () => import('@/views/vipproduct/companyRight'), name: 'CompanyRight', meta: { title: '企业权益' }, noCache: true }
    ]
  },
  {
    path: '/vip/user/list',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: 'list',
    meta: {
      title: '会员管理', icon: 'user'
    },
    children: [
      { path: '/vip/user/list', component: () => import('@/views/vip/vipUserList'), name: 'VipUserList', meta: { title: '会员订单' }, noCache: true }
    ]
  },
  {
    path: '/huodong',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/huodong/list',
    meta: {
      title: '活动', icon: 'table'
    },
    children: [
      { path: '/huodong/list', component: () => import('@/views/huodong/list'), name: 'HuodongList', meta: { title: '活动' }, noCache: true },
      { path: '/huodong/edit/:hd_sn(\\S+)', component: () => import('@/views/huodong/edit'), name: 'HuodongEdit', meta: { title: '活动详情' }, noCache: false, hidden: true },
      { path: '/huodong/add', component: () => import('@/views/huodong/add'), name: 'HuodongAdd', meta: { title: '新建活动' }, noCache: true },
      { path: '/huodong/joinedlist/:hd_sn(\\S+)', component: () => import('@/views/huodong/joinedlist'), name: 'Joinedlist', meta: { title: '活动报名' }, noCache: false, hidden: true }
    ]
  },
  {
    path: '/quanzi',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/quanzi/list',
    meta: {
      title: '圈子', icon: 'nested'
    },
    children: [
      { path: '/quanzi/list', component: () => import('@/views/quanzi/list'), name: 'quanziList', meta: { title: '圈子' }, noCache: true },
      { path: '/quanzi/edit/:qz_sn(\\S+)', component: () => import('@/views/quanzi/edit'), name: 'quanziEdit', meta: { title: '圈子详情' }, noCache: false, hidden: true },
      { path: '/quanzi/add', component: () => import('@/views/quanzi/add'), name: 'quanziAdd', meta: { title: '创建圈子' }, noCache: true },
      { path: '/quanzi/qzclientlist/:qz_sn(\\S+)', component: () => import('@/views/quanzi/qzClientList'), name: 'QzClientList', meta: { title: '圈子客户资源' }, noCache: false, hidden: true },
      { path: '/quanzi/qzorderlist/:qz_sn(\\S+)', component: () => import('@/views/quanzi/qzOrderList'), name: 'qzOrderList', meta: { title: '圈子成员' }, noCache: true },
      { path: '/quanzi/userapply', component: () => import('@/views/quanzi/userApply'), name: 'UserApply', meta: { title: '用户申请' }, noCache: true }
    ]
  },
  {
    path: '/zhaopin',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/zhaopin/list',
    meta: {
      title: '招聘', icon: 'tab'
    },
    children: [
      { path: '/zhaopin/list', component: () => import('@/views/zhaopin/list'), name: 'ZhaopinList', meta: { title: '职位' }, noCache: true },
      { path: '/zhaopin/edit/:zp_sn(\\S+)', component: () => import('@/views/zhaopin/edit'), name: 'ZhaopinEdit', meta: { title: '职位详情' }, noCache: false, hidden: true },
      { path: '/zhaopin/add/:company_sn(\\S+)', component: () => import('@/views/zhaopin/add'), name: 'ZhaopinAdd', meta: { title: '发布职位' }, noCache: true, hidden: true },
      { path: '/company/list', component: () => import('@/views/company/list'), name: 'CompanyList', meta: { title: '公司' }, noCache: true },
      { path: '/company/edit/:company_sn(\\S+)', component: () => import('@/views/company/edit'), name: 'CompanyEdit', meta: { title: '公司详情' }, noCache: false, hidden: true },
      { path: '/company/add', component: () => import('@/views/company/add'), name: 'CompanyAdd', meta: { title: '创建公司' }, noCache: true }
    ]
  },
  {
    path: '/huzhu',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/huzhu/list',
    meta: {
      title: '互助', icon: 'guide'
    },
    children: [
      { path: '/huzhu/list', component: () => import('@/views/huzhu/huzhuList'), name: 'HuzhuList', meta: { title: '互助' }, noCache: true }
    ]
  },
  {
    path: '/citypartner/list',
    component: Layout,
    alwaysShow: false, // will always show the root menu
    redirect: 'list',
    meta: {
      title: '城市合伙人', icon: 'list'
    },
    children: [
      { path: '/citypartner/list', component: () => import('@/views/citypartner/partnerList'), name: 'PartnerList', meta: { title: '城市合伙人' }, noCache: true }
    ]
  },
  {
    path: '/invoice',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/invoice/list',
    meta: {
      title: '发票', icon: 'nested'
    },
    children: [
      { path: '/invoice/list', component: () => import('@/views/invoice/list'), name: 'InvoiceList', meta: { title: '发票' }, noCache: true }
    ]
  },
  /* {
    path: '/company',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: '/company/list',
    meta: {
      title: '公司', icon: 'component'
    },
    children: [
      { path: '/company/list', component: () => import('@/views/company/list'), name: 'CompanyList', meta: { title: '公司' }, noCache: true },
      { path: '/company/edit/:company_sn(\\S+)', component: () => import('@/views/company/edit'), name: 'CompanyEdit', meta: { title: '公司详情' }, noCache: false, hidden: true },
      { path: '/company/add', component: () => import('@/views/company/add'), name: 'CompanyAdd', meta: { title: '创建公司' }, noCache: true }
    ]
  },*/
  {
    path: '/platform',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    redirect: 'slideshow',
    meta: {
      title: '平台配置', icon: 'example'
    },
    children: [
      { path: 'slide/list', component: () => import('@/views/platform/slide/list'), name: 'Slidelist', meta: { title: '轮播图' }, noCache: true },
      { path: 'slide/create', component: () => import('@/views/platform/slide/create'), name: 'slide-create', meta: { title: '新建轮播图', noCache: false }, hidden: true },
      { path: 'slide/edit', component: () => import('@/views/platform/slide/edit'), name: 'slide-edit', meta: { title: '编辑轮播图', noCache: false }, hidden: true },
      { path: 'agreement/user', component: () => import('@/views/platform/agreement/useragreement'), name: 'Useragreement', meta: { title: '用户协议' }, noCache: true },
      { path: 'agreement/privacypolicy', component: () => import('@/views/platform/agreement/privacypolicy'), name: 'Privacypolicy', meta: { title: '隐私政策' }, noCache: true },
      { path: 'agreement/operationsguide', component: () => import('@/views/platform/agreement/operationsguide'), name: 'Operationsguide', meta: { title: '操作指南' }, noCache: true },
      { path: 'agreement/punishrule', component: () => import('@/views/platform/agreement/punishrule'), name: 'Punishrule', meta: { title: '平台处罚规则' }, noCache: true },
      { path: 'agreement/integralrule', component: () => import('@/views/platform/agreement/integralrule'), name: 'Integralrule', meta: { title: '积分规则' }, noCache: true },
      { path: 'department/department', component: () => import('@/views/platform/department/department'), name: 'Integralrule', meta: { title: '对接部门' }, noCache: true },
      { path: 'industry/industry', component: () => import('@/views/platform/industry/industry'), name: 'Industry', meta: { title: '客户类型' }, noCache: true },
      { path: 'product/product', component: () => import('@/views/platform/product/product'), name: 'Product', meta: { title: '产品类型' }, noCache: true }
    ]
  }
]
