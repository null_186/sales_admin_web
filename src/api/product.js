import request from '@/utils/request'

export function apiProductTypeTreeList(pData) {
  return request({
    url: '?r=product/type-tree-list',
    method: 'post',
    data: pData
  })
}

export function apiProductTypeAdd(pData) {
  return request({
    url: '?r=product/type-add',
    method: 'post',
    data: pData
  })
}

export function apiProductTypeDel(pData) {
  return request({
    url: '?r=product/type-del',
    method: 'post',
    data: pData
  })
}

