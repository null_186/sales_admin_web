/**
 * 圈子
 **/
import request from '@/utils/request'

export function apiQuanziSave(pData) {
  return request({
    url: '?r=quanzi/save',
    method: 'post',
    data: pData
  })
}

export function apiQuanziList(pData) {
  return request({
    url: '?r=quanzi/list',
    method: 'post',
    data: pData
  })
}

export function apiQuanziInfo(pData) {
  return request({
    url: '?r=quanzi/info',
    method: 'post',
    data: pData
  })
}

export function apiQuanziStatus(pData) {
  return request({
    url: '?r=quanzi/status',
    method: 'post',
    data: pData
  })
}

export function apiQuanziAddClient(pData) {
  return request({
    url: '?r=quanzi/add-client',
    method: 'post',
    data: pData
  })
}
export function apiQuanziDelClient(pData) {
  return request({
    url: '?r=quanzi/del-client',
    method: 'post',
    data: pData
  })
}

export function apiQuanziClientList(pData) {
  return request({
    url: '?r=quanzi/client-list',
    method: 'post',
    data: pData
  })
}

export function apiQuanziOrderList(pData) {
  return request({
    url: '?r=quanzi/order-list',
    method: 'post',
    data: pData
  })
}

export function apiQuanziOrderCheckStatus(pData) {
  return request({
    url: '?r=quanzi/order-check-status',
    method: 'post',
    data: pData
  })
}

export function apiQuanziOrderRefund(pData) {
  return request({
    url: '?r=quanzi/order-refund',
    method: 'post',
    data: pData
  })
}

export function apiQuanziListByClient(pData) {
  return request({
    url: '?r=quanzi/list-by-client',
    method: 'post',
    data: pData
  })
}

export function apiQuanziRefreshQR(pData) {
  return request({
    url: '?r=quanzi/refresh-qr',
    method: 'post',
    data: pData
  })
}

export function apiQuanziUserApplyList(pData) {
  return request({
    url: '?r=quanzi/user-apply-list',
    method: 'post',
    data: pData
  })
}

export function apiQuanziOrderSort(pData) {
  return request({
    url: '?r=quanzi/order-sort',
    method: 'post',
    data: pData
  })
}

export function apiQuanziClientSort(pData) {
  return request({
    url: '?r=quanzi/client-sort',
    method: 'post',
    data: pData
  })
}

export function apiQuanziOrderAddUser(pData) {
  return request({
    url: '?r=quanzi/order-add-user',
    method: 'post',
    data: pData
  })
}

