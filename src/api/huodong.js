/**
 * 活动
 **/
import request from '@/utils/request'

export function apiHuodongSave(pData) {
  return request({
    url: '?r=huodong/save',
    method: 'post',
    data: pData
  })
}

export function apiHuodongList(pData) {
  return request({
    url: '?r=huodong/list',
    method: 'post',
    data: pData
  })
}

export function apiHuodongInfo(pData) {
  return request({
    url: '?r=huodong/info',
    method: 'post',
    data: pData
  })
}

export function apiHuodongJoinedList(pData) {
  return request({
    url: '?r=huodong/joined-list',
    method: 'post',
    data: pData
  })
}

export function apiHuodongJoinedAudit(pData) {
  return request({
    url: '?r=huodong/joined-audit',
    method: 'post',
    data: pData
  })
}

export function apiHuodongRefreshQR(pData) {
  return request({
    url: '?r=huodong/refresh-qr',
    method: 'post',
    data: pData
  })
}

export function apiHuodongStatus(pData) {
  return request({
    url: '?r=huodong/status',
    method: 'post',
    data: pData
  })
}

export function apiHuodongOrderRefund(pData) {
  return request({
    url: '?r=huodong/order-refund',
    method: 'post',
    data: pData
  })
}
