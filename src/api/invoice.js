import request from '@/utils/request'

export function apiInvoiceList(pData) {
  return request({
    url: '?r=invoice/list',
    method: 'post',
    data: pData
  })
}

export function apiInvoiceSet(pData) {
  return request({
    url: '?r=invoice/set',
    method: 'post',
    data: pData
  })
}
