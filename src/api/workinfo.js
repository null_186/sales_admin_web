/**
 * 招聘
 **/
import request from '@/utils/request'

export function apiWorkinfoWorkExperienceArr() {
  return request({
    url: '?r=workinfo/work-experience-arr',
    method: 'get'
  })
}

export function apiWorkinfoEducationArr() {
  return request({
    url: '?r=workinfo/education-arr',
    method: 'get'
  })
}
