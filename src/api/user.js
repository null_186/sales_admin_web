/**
 * 用户相关
 **/
import request from '@/utils/request'

export function apiUserList(pData) {
  return request({
    url: '?r=user/list',
    method: 'post',
    data: pData
  })
}

export function apiUserStatus(pData) {
  return request({
    url: '?r=user/status',
    method: 'post',
    data: pData
  })
}

export function apiUserDetail(user_id) {
  return request({
    url: '?r=user/detail',
    method: 'get',
    params: { user_id }
  })
}

export function apiUserSetIntegral(pData) {
  return request({
    url: '?r=user/set-integral',
    method: 'get',
    params: pData
  })
}

export function apiUserSetCompanyName(pData) {
  return request({
    url: '?r=user/set-company-name',
    method: 'get',
    params: pData
  })
}

export function apiUserIntegralList(pData) {
  return request({
    url: '?r=user/integral-list',
    method: 'get',
    params: pData
  })
}

export function apiUserClientList(pData) {
  return request({
    url: '?r=user/client-list',
    method: 'get',
    params: pData
  })
}

export function apiUserSaveClientInfo(pData) {
  return request({
    url: '?r=user/save-client-info',
    method: 'post',
    data: pData
  })
}

export function apiUserSetClientStatus(pData) {
  return request({
    url: '?r=user/set-client-status',
    method: 'get',
    params: pData
  })
}

export function apiUserSaveRecommend(pData) {
  return request({
    url: '?r=user/save-recommend',
    method: 'get',
    params: pData
  })
}

export function apiUserSaveRank(pData) {
  return request({
    url: '?r=user/save-rank',
    method: 'get',
    params: pData
  })
}

export function apiUserDelClient(pData) {
  return request({
    url: '?r=user/del-client',
    method: 'get',
    params: pData
  })
}

export function apiUserCommentList(pData) {
  return request({
    url: '?r=user/comment-list',
    method: 'get',
    params: pData
  })
}

export function apiUserSetClasses(pData) {
  return request({
    url: '?r=user/set-classes',
    method: 'get',
    params: pData
  })
}

export function apiUserJubaoList(pData) {
  return request({
    url: '?r=user/jubao-list',
    method: 'get',
    params: pData
  })
}

export function apiUserOrder(pData) {
  return request({
    url: '?r=user/user-order',
    method: 'get',
    params: pData
  })
}

export function apiUserBaseInfoByPhone(pData) {
  return request({
    url: '?r=user/base-info-by-phone',
    method: 'get',
    params: pData
  })
}

export function apiUserSaveUserInfo(pData) {
  return request({
    url: '?r=user/save-user-info',
    method: 'get',
    params: pData
  })
}

export function apiUserGiveVip(pData) {
  return request({
    url: '?r=user/give-vip',
    method: 'post',
    data: pData
  })
}

