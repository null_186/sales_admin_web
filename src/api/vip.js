/**
 * 会员相关
 **/
import request from '@/utils/request'

export function apiVipUserList(pData) {
  return request({
    url: '?r=vip/user-list',
    method: 'post',
    data: pData
  })
}

export function apiVipRefund(pData) {
  return request({
    url: '?r=vip/refund',
    method: 'post',
    data: pData
  })
}

