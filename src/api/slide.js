/**
 * 轮播图
 **/
import request from '@/utils/request'

export function updateSlide(pData) {
  return request({
    url: '?r=slide/update',
    method: 'post',
    data: pData
  })
}

export function listSlide() {
  return request({
    url: '?r=slide/list',
    method: 'get'
  })
}

export function changeStatus(slide_id, status) {
  return request({
    url: '?r=slide/change-status',
    method: 'post',
    data: { slide_id: slide_id, status: status }
  })
}

export function changeOrder(slide_id, order) {
  return request({
    url: '?r=slide/change-order',
    method: 'post',
    data: { slide_id: slide_id, sort_order: order }
  })
}

export function infoSlide(slide_id) {
  return request({
    url: '?r=slide/info',
    method: 'post',
    data: { slide_id: slide_id }
  })
}

export function deleteSlide(slide_id) {
  return request({
    url: '?r=slide/del',
    method: 'post',
    data: { slide_id: slide_id }
  })
}
