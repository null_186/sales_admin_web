/**
 * 积分
 **/
import request from '@/utils/request'

export function apiIntegralList(pData) {
  return request({
    url: '?r=integral/list',
    method: 'post',
    data: pData
  })
}

export function apiIntegralSave(pData) {
  return request({
    url: '?r=integral/save',
    method: 'post',
    data: pData
  })
}

export function apiIntegralDel(integral_product_id) {
  return request({
    url: '?r=integral/del',
    method: 'get',
    params: { integral_product_id }
  })
}

export function apiIntegralOrderList(param) {
  return request({
    url: '?r=integral/order-list',
    method: 'get',
    params: param
  })
}
