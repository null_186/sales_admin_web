/**
 * 公司
 **/
import request from '@/utils/request'

export function apiCompanyProductTypeList() {
  return request({
    url: '?r=company/product-type-tree-list',
    method: 'get'
  })
}

export function apiCompanyScale() {
  return request({
    url: '?r=company/scale',
    method: 'get'
  })
}

export function apiCompanySave(pData) {
  return request({
    url: '?r=company/save',
    method: 'post',
    data: pData
  })
}

export function apiCompanyList(pData) {
  return request({
    url: '?r=company/list',
    method: 'post',
    data: pData
  })
}

export function apiCompanyInfo(pData) {
  return request({
    url: '?r=company/info',
    method: 'post',
    data: pData
  })
}

export function apiCompanyDel(pData) {
  return request({
    url: '?r=company/del',
    method: 'post',
    data: pData
  })
}
