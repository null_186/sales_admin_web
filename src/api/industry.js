import request from '@/utils/request'

export function apiIndustryList(pData) {
  return request({
    url: '?r=industry/list',
    method: 'post',
    data: pData
  })
}

export function apiIndustryAdd(pData) {
  return request({
    url: '?r=industry/add',
    method: 'post',
    data: pData
  })
}

export function apiIndustryDel(pData) {
  return request({
    url: '?r=industry/del',
    method: 'post',
    data: pData
  })
}

