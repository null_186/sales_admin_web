/**
 * 合伙人
 **/
import request from '@/utils/request'

export function apiCityPartnerList(pData) {
  return request({
    url: '?r=citypartner/list',
    method: 'post',
    data: pData
  })
}
