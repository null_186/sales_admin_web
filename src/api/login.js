import request from '@/utils/request'

export function loginByUsername(username, password, device) {
  const data = {
    username,
    password,
    device
  }
  return request({
    url: '?r=admin/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/login/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '?r=admin/info',
    method: 'get',
    params: { token }
  })
}

