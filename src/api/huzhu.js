/**
 * 互助
 **/
import request from '@/utils/request'

export function apiHuzhuList(pData) {
  return request({
    url: '?r=huzhu/list',
    method: 'post',
    data: pData
  })
}

export function apiHuzhuRefreshQR(pData) {
  return request({
    url: '?r=huzhu/refresh-qr',
    method: 'post',
    data: pData
  })
}

export function apiHuzhuStatus(pData) {
  return request({
    url: '?r=huzhu/status',
    method: 'post',
    data: pData
  })
}
