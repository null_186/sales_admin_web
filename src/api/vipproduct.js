import request from '@/utils/request'

export function apiVipProductList(pData) {
  return request({
    url: '?r=vipproduct/list',
    method: 'post',
    data: pData
  })
}

export function apiVipProductSave(pData) {
  return request({
    url: '?r=vipproduct/save',
    method: 'post',
    data: pData
  })
}
export function apiVipProductUserRight(pData) {
  return request({
    url: '?r=vipproduct/user-right',
    method: 'post',
    data: pData
  })
}
export function apiVipProductUserRightSave(pData) {
  return request({
    url: '?r=vipproduct/user-right-save',
    method: 'post',
    data: pData
  })
}

export function apiVipProductCompanyRight(pData) {
  return request({
    url: '?r=vipproduct/company-right',
    method: 'post',
    data: pData
  })
}
export function apiVipProductCompanyRightSave(pData) {
  return request({
    url: '?r=vipproduct/company-right-save',
    method: 'post',
    data: pData
  })
}

export function apiVipProductMonth(pData) {
  return request({
    url: '?r=vipproduct/month',
    method: 'post',
    data: pData
  })
}
