/**
 * 系统配置
 **/
import request from '@/utils/request'

export function apiConfigAgreement(pData) {
  return request({
    url: '?r=config/agreement',
    method: 'post',
    data: pData
  })
}

export function apiConfigClientTypeList() {
  return request({
    url: '?r=config/client-type-list',
    method: 'get'
  })
}

export function apiConfigCompanySizeList() {
  return request({
    url: '?r=config/company-size-list',
    method: 'get'
  })
}

export function apiConfigCoopTitleList() {
  return request({
    url: '?r=config/coop-title-list',
    method: 'get'
  })
}

export function apiConfigCoopDepartmentList() {
  return request({
    url: '?r=config/coop-department-list',
    method: 'get'
  })
}

export function apiConfigDepartmentList() {
  return request({
    url: '?r=config/department',
    method: 'get'
  })
}

export function apiConfigDepartmentSave(pData) {
  return request({
    url: '?r=config/department-save',
    method: 'post',
    data: pData
  })
}

export function apiConfigDepartmentDel(pData) {
  return request({
    url: '?r=config/department-del',
    method: 'post',
    data: pData
  })
}
