import request from '@/utils/request'

export function getToken() {
  return request({
    url: '?r=qiniu/token', // 假地址 自行替换
    method: 'get'
  })
}

export function getImgFile(key) {
  return request({
    url: process.env.QINIU_SITE + key,
    method: 'get'
  })
}
