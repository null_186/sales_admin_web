/**
 * 招聘
 **/
import request from '@/utils/request'

export function apiZhaopinList(pData) {
  return request({
    url: '?r=zhaopin/list',
    method: 'post',
    data: pData
  })
}

export function apiZhaopinSave(pData) {
  return request({
    url: '?r=zhaopin/save',
    method: 'post',
    data: pData
  })
}

export function apiZhaopinDel(pData) {
  return request({
    url: '?r=zhaopin/del',
    method: 'post',
    data: pData
  })
}

export function apiZhaopinInfo(pData) {
  return request({
    url: '?r=zhaopin/info',
    method: 'post',
    data: pData
  })
}
